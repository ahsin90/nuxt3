// store/validationError.ts
import { defineStore } from "pinia";

export const validationErrors = defineStore({
  id: "validationErrors",
  state: () => ({
    errors: [],
  }),
  actions: {
    setErrors(err) {
      this.errors = err;
    },
    clearErrors() {
      this.errors = [];
    },
  },
});
