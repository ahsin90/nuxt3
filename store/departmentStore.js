// store/department.js
import { defineStore } from "pinia";

export const departmentStore = defineStore({
  id: "departmentStore",
  state: () => ({
    totalDepartments: null,
    totalUsersInDepartment: null,
    totalUsersNotInDepartment: null,
    modalShow: false,
    allUsers: null,
    usersIndepartment: null,
  }),
  actions: {
    setTotalDepartments(value) {
      this.totalDepartments = value;
    },
    setTotalUsersInDepartment(value) {
      this.totalUsersInDepartment = value;
    },
    setTotalUsersNotInDepartment(value) {
      this.totalUsersNotInDepartment = value;
    },
    setModalShow(value) {
      this.modalShow = value;
    },
    setAllUsers(data) {
      this.allUsers = [];
      this.allUsers.push(data);
    },
    setUsersIndepartment(data) {
      this.usersIndepartment = [];
      this.usersIndepartment.push(data);
    },
    clearAll() {
      this.totalDepartments = null;
      this.totalUsersInDepartment = null;
      this.totalUsersNotInDepartment = null;
    },
  },
});
