// store/department.js
import { defineStore } from "pinia";

export const dayOffStore = defineStore({
  id: "dayOffStore",
  state: () => ({
    dayOffList: null,
    allUsersList: null,
    userListInDayOff: null,
    modalShow: false,
  }),
  actions: {
    setDayOffList(data) {
      this.dayOffList = [];
      this.dayOffList.push(data);
    },
    setAllUsersList(data) {
      this.allUsersList = [];
      this.allUsersList.push(data);
    },
    setUserListInDayOff(data) {
      this.userListInDayOff = [];
      this.userListInDayOff.push(data);
    },
    removeUserListInDayOff() {
      this.userListInDayOff = null;
    },
    setModalShow(value) {
      this.modalShow = value;
    },
  },
});
