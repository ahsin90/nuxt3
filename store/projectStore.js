// store/department.js
import { defineStore } from "pinia";

export const projectStore = defineStore({
  id: "projectStore",
  state: () => ({
    totalProjects: null,
    totalProjectsOnGoing: null,
    modalShow: false,
    projectList: null,
    usersInProject: null,
    allUsers: null,
    projectHistory: null,
    activeStatus: null,
    activeSalesStatus: null,
    loader: false,
  }),
  actions: {
    setTotalProjects(value) {
      this.totalProjects = value;
    },
    setTotalProjectsOnGoing(value) {
      this.totalProjectsOnGoing = value;
    },
    setModalShow(value) {
      this.modalShow = value;
    },
    setProjectList(data) {
      this.projectList = [];
      this.projectList.push(data);
    },
    setUsersInProject(data) {
      this.usersInProject = [];
      this.usersInProject.push(data);
    },
    setAllUsers(data) {
      this.allUsers = [];
      this.allUsers.push(data);
    },
    setProjectHistory(data) {
      this.projectHistory = [];
      this.projectHistory.push(data);
    },
    setActiveStatus(status) {
      this.activeStatus = status;
    },
    setActiveSalesStatus(status) {
      this.activeSalesStatus = status;
    },
    setLoader(value) {
      this.loader = value;
    },
  },
});
