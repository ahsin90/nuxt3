// store/department.js
import { defineStore } from "pinia";

export const timeSheetsStore = defineStore({
  id: "timeSheetsStore",
  state: () => ({
    timeSheetsList: null,
    selectedDepartmentId: null,
    selectedDepartmentName: "All Department",
    loader: false,
  }),
  actions: {
    setTimeSheetsList(data) {
      this.timeSheetsList = [];
      this.timeSheetsList.push(data);
    },
    clearTimeSheetsList() {
      this.timeSheetsList = null;
    },
    setSelectedDepartmentId(value) {
      this.selectedDepartmentId = value;
    },
    setSelectedDepartmentName(value) {
      this.selectedDepartmentName = value;
    },
    setLoader(value) {
      this.loader = value;
    },
  },
});
