// store/department.js
import { defineStore } from "pinia";

export const workTypeStore = defineStore({
  id: "workTypeStore",
  state: () => ({
    workTypeList: null,
    usersInWorkType: null,
    totalUsers: null,
    totalHours: null,
  }),
  actions: {
    setWorkTypeList(data) {
      this.workTypeList = [];
      this.workTypeList.push(data);
    },
    setUsersInWorkType(data) {
      this.usersInWorkType = [];
      this.usersInWorkType.push(data);
    },
    setTotalUsers(value) {
      this.totalUsers = value;
    },
    setTotalHours(value) {
      this.totalHours = value;
    },
  },
});
