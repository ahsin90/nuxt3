// store/error.ts
import { defineStore } from "pinia";

export const mainStore = defineStore({
  id: "mainStore",
  state: () => ({
    selectedDateRange: {
      key: "today",
      value: "Today",
    },
    confirmModal: {
      show: false,
      text: null,
    },
    alertInfo: {
      show: false,
      text: null,
      type: "success",
    },
  }),
  actions: {
    setSelectedDateRange(value) {
      this.selectedDateRange.key = value.key;
      this.selectedDateRange.value = value.value;
    },
    setConfirmModal(value) {
      this.confirmModal.show = value.show;
      this.confirmModal.text = value.text;
    },
    setAlertInfo(value) {
      this.alertInfo.show = value.show;
      this.alertInfo.text = value.text;
      this.alertInfo.type = value.type;
    },
    closeAlertInfo() {
      this.alertInfo.show = false;
      this.alertInfo.text = null;
      this.alertInfo.type = "success";
    },
  },
});
