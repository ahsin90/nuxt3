// store/userStore.js
import { defineStore } from "pinia";

export const userStore = defineStore({
  id: "userStore",
  state: () => ({
    userList: null,
  }),
  actions: {
    setUserList(data) {
      this.userList = [];
      this.userList.push(data);
    },
  },
});
