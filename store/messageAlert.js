// store/error.ts
import { defineStore } from "pinia";

export const messageAlert = defineStore({
  id: "messageAlert",
  state: () => ({
    message: null,
    type: null,
  }),
  actions: {
    setMessageAlert(msg, type) {
      this.message = msg;
      this.type = type;
    },
    clearMessageAlert() {
      (this.message = null), (this.type = null);
    },
  },
});
