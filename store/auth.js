// store/auth.ts
import { defineStore } from "pinia";

export const useAuthStore = defineStore({
  id: "authStore",
  state: () => ({
    user: null,
    token: null,
    refreshToken: null,
  }),
  persist: {
    storage: persistedState.localStorage,
  },
  actions: {
    async login(userData) {
      /* Update Pinia state */
      this.user = userData;
      this.token = userData.token;
      this.refreshToken = userData.refreshToken;
    },
    async updateUserData(userData) {
      this.user = userData;
    },
    async updateToken(newToken) {
      this.token = newToken;
    },
    async updateRefreshToken(newRefreshToken) {
      this.refreshToken = newRefreshToken;
    },
    logout() {
      this.user = null;
      this.token = null;
      this.refreshToken = null;
    },
  },
});
