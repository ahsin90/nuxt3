// store/error.ts
import { defineStore } from "pinia";

export const loader = defineStore({
  id: "loader",
  state: () => ({
    display: false,
  }),
  actions: {
    setDisplay(status) {
      this.display = status;
    },
  },
});
