FROM node:18

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Copying files
COPY . /app

RUN npm install 
RUN npm run build

RUN rm -rf node_modules

EXPOSE 3000

# Running the app
CMD ["node", "./.output/server/index.mjs"]
