import { defineNuxtPlugin } from "#app";
import { mainStore } from "~/store/mainStore";

// alert.closeAlertInfo();
// import Toast from "vue-toastification";
// // import Toast, { POSITION, useToast } from "vue-toastification";
// import "vue-toastification/dist/index.css";
// import { useToast } from "vue-toastification";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.provide("showToast", (msg, type, time = 5000) => {
    const alert = mainStore();
    alert.setAlertInfo({
      show: true,
      text: msg,
      type: "success",
    });
  });
});
