import { defineNuxtPlugin } from "#app";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.provide("hourRound", (time) => {
    if (time) {
      // The time format must be like 00:30:00
      const split = time.split(":");
      let hours, minutes;
      hours = split[0];

      // convert hours to minutes
      const hoursToMinutes = Math.floor(parseInt(hours) * 60);
      minutes = parseInt(split[1]);

      const totalMinutes = hoursToMinutes + minutes;
      let totalHours = totalMinutes / 60;
      totalHours = totalHours.toFixed(2);
      return totalHours;
    } else {
      return 0;
    }
  });
});
