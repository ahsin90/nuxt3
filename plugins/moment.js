import { defineNuxtPlugin } from "#app";
import moment from "moment";

export default defineNuxtPlugin((nuxtApp) => {
  //   nuxtApp.vueApp.use(VueMoment);

  nuxtApp.provide("moment", (time) => moment(time));
});
