/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    // "./node_modules/flowbite/**/*.js",
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#9C268F',
        'primary-light': '#D553C7',
        'secondary': '#32174A',
        'primary-vogue': '#F7EEF6',
        'secondary-vogue': '#EFEDF1',
        'success-vogue': '#F4F9E9',
        'success': '#78A518',
        'danger-vogue': '#FCF1EE',
        'danger': '#D04D23',
        'warning': '#E9B123',
        'warning-vogue': '#FEFBF4',
      },
      flex: {
        '2': '2 2 0%'
      }
    },
    fontFamily: {
      'display': ['Signika', 'sans-serif'],
      'body': ['Signika', 'sans-serif'],
    },
  },
  // plugins: [
  //   require('flowbite/plugin')
  // ]

}
