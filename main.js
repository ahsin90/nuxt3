import { createApp } from 'vue';
import App from './App.vue';
import Datepicker from '@vuepic/vue-datepicker';
// import '@vuepic/vue-datepicker/dist/main.css'
import 'flowbite';
import VueClickAway from "vue3-click-away";

// add this
import './../style/main.css'

const app = createApp(App);
app.use(VueClickAway);
app.component('Datepicker', Datepicker);
app.mount('#app');