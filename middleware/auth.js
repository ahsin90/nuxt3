import { useAuthStore } from "~/store/auth";

export default defineNuxtRouteMiddleware(async () => {
  const app = useNuxtApp();
  const auth = useAuthStore();
  const router = useRouter();
  const runtimeConfig = useRuntimeConfig();

  if (process.client) {
    // fetch to get current user, if unauthorize do refresh token. if fail, redirect to login page
    await useLazyFetch(runtimeConfig.apiBaseUrl + "admin/getCurrentUser", {
      headers: {
        Authorization: `Bearer ${auth.token}`,
      },
      onResponse({ request, response, options }) {
        // Process the response data
        if (response.status === 401) {
          // do refresh token
          refreshToken();
        }
      },
    });

    const currentUrl = window.location;
    if (auth.user == null) {
      router.push("/?rel=" + currentUrl);
    }
  }
});

async function refreshToken() {
  const auth = useAuthStore();
  const runtimeConfig = useRuntimeConfig();
  const router = useRouter();
  const currentUrl = window.location;

  await useFetch(runtimeConfig.apiBaseUrl + "refresh-token", {
    method: "POST",
    body: { refreshToken: auth.user.refreshToken },
    onResponse({ request, response, options }) {
      if (response.status === 200) {
        const resData = response._data.data;
        auth.updateToken(resData.token);
        auth.updateRefreshToken(resData.refreshToken);

        console.log("The token refreshed with " + resData.token);
      } else {
        // force logout
        auth.logout();
        router.push("/?rel=" + currentUrl);
      }
    },
  });
}
