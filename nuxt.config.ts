// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    // The private keys which are only available server-side
    apiSecret: "",
    // Keys within public are also exposed client-side
    public: {
      // apiBaseUrl: "http://localhost:4000/",
      apiBaseUrl: "http://103.179.254.81:4000/",
      reCaptchaSiteKey: "6Lc4GHcjAAAAANQNbhv3YFhfGdTtVR637ThGjB6B",
    },
  },
  css: ["@/style/main.css", "@/style/custom.scss"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  modules: [
    [
      "@pinia/nuxt",
      {
        autoImports: ["defineStore", "acceptHMRUpdate"],
      },
    ],
    "@pinia-plugin-persistedstate/nuxt",
  ],
});
